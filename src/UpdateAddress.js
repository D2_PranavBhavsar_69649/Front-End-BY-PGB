
    import axios from "axios";
import {useContext, useState} from "react";
import { useLocation, useNavigate } from "react-router-dom";
import {UserContext} from "./contexts/userContext";

export default function UpdateAddress()
{
    const location = useLocation();

    const {loggedinuser} = useContext(UserContext);
    console.log(loggedinuser)

    const {setloggedinuser} = useContext(UserContext);

    const [address, setaddress] = useState(JSON.parse(location.state).address);

    const navigate = useNavigate();

    

    const HandleChange = (args) => {
        const copyofaddress = {...address};
        copyofaddress[args.target.name] = args.target.value;
        setaddress(copyofaddress);
        console.log(address);
    };

    const UpdateAddress = () => {
        const url = "http://localhost:8080/address/update/" + address.aid;
        console.log(address);
        axios.post(url, address)
        .then(()=>console.log("User updated!"))
        .then(()=>navigate('/allUsers'));        };

    // const onHandleChange = (e) => {
    //     setrole(x => e.target.value);
    //     console.log(role);
    //     // e.target.value
    //     //console.log(e.target.value);
    // }

    return(
        <div className={'FormContainer'}>
            <center>
                <h2>Update Address</h2>
            </center>
        <div className={'FieldContainer'}>
            <label className={'FieldLabel'}>
                Flat No:
                <br/>
                <input name={'flatNo'} type={'text'} 
                       value={address.flatNo} onChange={HandleChange}
                />
            </label>
            <div>
                <label className={'FieldLabel'}>
                    Street:
                    <br/>
                    <input name={'street'} type={'text'}  
                           value={address.street} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    Landmark:
                    <br/>
                    <input name={'landmark'} type={'text'} 
                           value={address.landmark} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    City:
                    <br/>
                    <input name={'city'} type={'text'}  
                           value={address.city} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    PinCode:
                    <br/>
                    <input name={'pincode'} type={'text'} 
                           value={address.pincode} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    <input name={'submit'} type={'submit'}
                           value={'Update Address'} onClick={UpdateAddress}
                    />
                </label>
            </div>
        </div>
        </div>
    )
}
