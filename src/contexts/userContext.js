import { createContext, useState } from "react";
export const UserContext = createContext({});

export default function UserProvider({children}) {
    
    const[loggedinuser, setloggedinuser] = useState({});
    return (
        <UserContext.Provider value={{loggedinuser, setloggedinuser}}>{children}</UserContext.Provider>
    )
}