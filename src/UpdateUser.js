import axios from "axios";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

export default function UpdateUser() {

    const navigate = useNavigate();

    const location = useLocation();

    const [user, setuser] = useState(JSON.parse(location.state));

    const HandleChange =(args)=> {
        let {name, value} = args.target;
        setuser({...user, [name]: value});
    }

    const UpdateUser = () => {
        axios.post("http://localhost:8080/user/update/"+user.uid,user)
            .then(()=>console.log("User updated!"))
            .then(()=>navigate('/allUsers'));
    
}
return(
    <div className={'FormContainer'}>
        <center>
            <h2>Register</h2>
        </center>
    <div className={'FieldContainer'}>
        <label className={'FieldLabel'}>
            Username:
            <br/>
            <input name={'uname'} type={'text'} 
                   value={user.uname} onChange={HandleChange}
            />
        </label>
        <div>
            <label className={'FieldLabel'}>
                Password:
                <br/>
                <input name={'passwd'} type={'password'} 
                       value={user.passwd} onChange={HandleChange}
                />
            </label>
        </div>
        <div>
            <label className={'FieldLabel'}>
                First Name:
                <br/>
                <input name={'firstName'} type={'text'}
                       value={user.firstName} onChange={HandleChange}
                />
            </label>
        </div>
        <div>
            <label className={'FieldLabel'}>
                Last Name:
                <br/>
                <input name={'lastName'} type={'text'} 
                       value={user.lastName} onChange={HandleChange}
                />
            </label>
        </div>
        <div>
            <label className={'FieldLabel'}>
                Mobile Number:
                <br/>
                <input name={'mob'} type={'text'} 
                       value={user.mob} onChange={HandleChange}
                />
            </label>
        </div>
        <div>
            <label className={'FieldLabel'}>
                Date of Birth:
                <br/>
                <input name={'dob'} type={'date'} 
                       min={"1900-01-01"} max={"2050-12-31"}
                       value={user.dob} onChange={HandleChange}
                />
            </label>
        </div>
        <div>
            <label className={'FieldLabel'}>
                <input name={'submit'} type={'submit'}
                       value={'Update User'} onClick={UpdateUser}
                />
            </label>
        </div>
    </div>
    </div>
)
}