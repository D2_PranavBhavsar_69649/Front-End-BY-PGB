import axios from "axios";
import {useContext, useState} from "react";
import {UserContext} from "./contexts/userContext";

export default function AddAddress()
{
    var [address, setaddress] = useState({flatNo: "", street: "", landmark: "", city: "", pincode: ""});

    const {loggedinuser} = useContext(UserContext);
    console.log(loggedinuser)

    const {setloggedinuser} = useContext(UserContext);

    const HandleChange = (args) => {
        const copyaddress = {...address};
        copyaddress[args.target.name] = args.target.value;
        setaddress(copyaddress);
        console.log(address);
    };

    const submitAddress = () => {
        const url = "http://localhost:8080/address/add/user/" + loggedinuser.uid;
        console.log(loggedinuser);
        axios.post(url, address).then((res) => {
            console.log(res);
        })
    };

    // const onHandleChange = (e) => {
    //     setrole(x => e.target.value);
    //     console.log(role);
    //     // e.target.value
    //     //console.log(e.target.value);
    // }

    return(
        <div className={'FormContainer'}>
            <center>
                <h2>Add Address</h2>
            </center>
        <div className={'FieldContainer'}>
            <label className={'FieldLabel'}>
                Flat No:
                <br/>
                <input name={'flatNo'} type={'text'}  placeholder={"enter your FlatNo"}
                       value={address.flatNo} onChange={HandleChange}
                />
            </label>
            <div>
                <label className={'FieldLabel'}>
                    Street:
                    <br/>
                    <input name={'street'} type={'text'}  placeholder={"enter street name"}
                           value={address.street} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    Landmark:
                    <br/>
                    <input name={'landmark'} type={'text'}  placeholder={"enter landmark"}
                           value={address.landmark} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    City:
                    <br/>
                    <input name={'city'} type={'text'}  placeholder={"Enter your city"}
                           value={address.city} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    PinCode:
                    <br/>
                    <input name={'pincode'} type={'text'}  placeholder={"000000"}
                           value={address.pincode} onChange={HandleChange}
                    />
                </label>
            </div>
            <div>
                <label className={'FieldLabel'}>
                    <input name={'submit'} type={'submit'}
                           value={'Add Address'} onClick={submitAddress}
                    />
                </label>
            </div>
        </div>
        </div>
    )
}