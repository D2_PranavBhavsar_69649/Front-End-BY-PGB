import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "./contexts/userContext";
import UpdateAddress from "./UpdateAddress";

export default function AllUsers(){

    const {loggedinuser} = useContext(UserContext);

    const {setloggedinuser} = useContext(UserContext);

    var [users, setusers]  = useState([]);

    const navigate = useNavigate();

    // var [user, setuser] = useState({uname: "", passwd: "", firstName: "", lastName: "", mob: "", dob: ""})

    useEffect(() => {
        const url = "http://localhost:8080/user/all";
        axios.get(url)
            .then((res) => {
                setusers(() => (res.data));
                console.log(users);
            })
    }
    , []);

    function UpdateUser(u) {
        navigate('/updateuser', {state: JSON.stringify(u)})
    }

    function UpdateAddress(u) {
        navigate('/updateaddress', {state: JSON.stringify(u)})
    }
    

    // function Update() {
    //     UpdateUser();
    //     UpdateAddress();
    // }


    return(<><table className='table table-bordered'>
                            <tbody>
                            {
                                users.map(u=>
                                {
                                    console.log(u.address);
                                        
                                            return (<tr className="{FormContainer}" key={u.uid}>
                                                <td className={'FieldLabel'}>
                                                    {u.uname}
                                                </td>
                                                <td className={'FieldLabel'}>
                                                    {u.passwd}
                                                </td>
                                                <td className={'FieldLabel'}>
                                                    {u.firstName}
                                                </td>
                                                <td className={'FieldLabel'}>
                                                    {u.lastName}
                                                </td>
                                                <td className={'FieldLabel'}>
                                                    {u.mob}
                                                </td>
                                                <td className={'FieldLabel'}>
                                                    {u.dob}
                                                </td>
                                                <td>
                                    <center>
                                    <button className='btn btn-info'
                                            onClick={() => UpdateUser(u)}
                                            >
                                        ✏️
                                    </button>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                    <button className='btn btn-info'
                                            // onClick={() => DeleteUser(u)}
                                            >
                                        ❌
                                    </button>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                    <button className='btn btn-info'
                                            onClick={() => UpdateAddress(u)}
                                            >
                                        Update Address
                                    </button>
                                    </center>
                                </td>
                                            </tr>)
                                        
                                })
                            }
                            </tbody>
                        </table>
</>);
}
