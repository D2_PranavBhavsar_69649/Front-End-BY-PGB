import {Link} from "react-router-dom";
import {useContext} from "react";

export default function NavBar() {

    return (
        <>
            <nav className={'Nav'}>
                <Link to={'/'} className={'SiteTitle'}>TDAMS 🍔🍟🌭</Link>
                <ul>
                    <li>
                        <Link to={'/all-products'}>Stock</Link>
                    </li>
                    <li>
                        <Link to={'/address'}>Address</Link>
                    </li>
                    <li>
                        <Link to={'/login'}> Login</Link>
                    </li>
                    <li>
                        <Link to={'/about'}>About</Link>
                    </li>
                    <li>
                        <Link to={'/allUsers'}>All Users</Link>
                    </li>
                    <li>
                        <Link to={'/register'}>Register</Link>
                    </li>
                </ul>
            </nav>

        </>
    )
}