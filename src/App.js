import Home from "./Home";
import NavBar from "./NavBar";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import About from "./About";
import Login from "./login";
import Register from "./register";
import AllItems from "./AllItems";
import AllUsers from "./AllUsers";
import Address from "./Address";
import UserProvider from "./contexts/userContext";
import UpdateUser from "./UpdateUser";
import UpdateAddress from "./UpdateAddress";

export default function App() {

  return (
        <UserProvider>
            <BrowserRouter>
                <NavBar loginState={false}></NavBar>
                <div>
                  <Routes>
                    <Route path={'/login'} element={<Login/>}></Route>
                    <Route path={'/address'} element={<Address/>}></Route>
                    <Route path={'/'} element={<Home/>}></Route>
                    <Route path={'/updateuser'} element={<UpdateUser/>}></Route>
                    <Route path={'/updateaddress'} element={<UpdateAddress/>}></Route>
                    <Route path={'/home'} element={<Home/>}></Route>
                    <Route path={'/products'} element={<AllItems/>}></Route>
                    <Route path={'/about'} element={<About/>}></Route>
                    <Route path={'/register'} element={<Register/>}></Route>
                    <Route path={'/allUsers'} element={<AllUsers/>}></Route>
                  </Routes>
                </div>
            </BrowserRouter>
        </UserProvider>
  );
}
